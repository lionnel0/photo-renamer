﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renamer
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FileDataSet m_DataSet = new FileDataSet();
        private int m_Index = 0;
        public MainWindow()
        {
            InitializeComponent();


            this.Loaded += OnLoaded;
        }

        protected void OnLoaded(Object sender, RoutedEventArgs e)
        {
            try
            {
				System.IO.DirectoryInfo l_dir = null;
				// Looking for Clipboard content
				object clipboard = Clipboard.GetData(DataFormats.Text);
				if (clipboard is string)
				{
					l_dir = new System.IO.DirectoryInfo(clipboard.ToString());
					if (!l_dir.Exists)
					{
						l_dir = null;
					}
				}
				else
				{
					System.Collections.Specialized.StringCollection files = Clipboard.GetFileDropList();
					if (files.Count ==1)
					{
						l_dir = new System.IO.DirectoryInfo(files[0]);
						if (!l_dir.Exists)
						{
							l_dir = null;
						}
					}
				}

                if (l_dir!=null)
                {
					m_CurDirTextBox.Text = l_dir.FullName;

                    // DirTreeView
                    this.m_DirTreeView.Items.Clear();
                    foreach (System.IO.DirectoryInfo dir in l_dir.GetDirectories())
                    {
                        this.m_DirTreeView.Items.Add(dir);
                    }

                    // FileListBox
                    this.m_FileListBox.DataContext = m_DataSet;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message + ex.StackTrace);
            }
        }

        private void DirChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_Index = 0;
                System.IO.DirectoryInfo l_curDir = new System.IO.DirectoryInfo(m_CurDirTextBox.Text);
                this.m_DataSet.Clear();
                if (l_curDir.Exists)
                {
                    foreach (System.IO.FileInfo l_file in l_curDir.GetFiles())
                    {
                        if (l_file.Name == "Thumbs.db") continue;
                        string newName = "";
                        if (BuildNewName(l_file, ref newName))
                        {
							this.m_DataSet.File.AddFileRow(l_file.Name != newName, l_curDir.FullName, l_file.Name, newName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message + ex.StackTrace);
            }
			this.m_FileListBox.DataContext = m_DataSet;
        }

        private bool BuildNewName(System.IO.FileInfo oldName, ref string newName)
        {
            string template = this.m_TemplateTextBox.Text;

            string text = template.Replace("%old&ext", oldName.Name).Replace("%old-ext", oldName.Name.Substring(0, oldName.Name.Length-oldName.Extension.Length));
            if (text.Contains("%index"))
            {
                m_Index += 1;
                text = text.Replace("%index02", m_Index.ToString("D2"));
                text = text.Replace("%index03", m_Index.ToString("D3"));
                text = text.Replace("%index", m_Index.ToString());
            }
            if (text.Contains("%EXIF"))
            {
                string textEXIF = "";
                System.Drawing.Image image = System.Drawing.Image.FromFile(oldName.FullName);
                foreach (System.Drawing.Imaging.PropertyItem item in image.PropertyItems)
                {
                    if (item.Id == 0x9003)
                    {
                        if (item.Type == 2)
                        {
                            char[] tab = ASCIIEncoding.ASCII.GetChars(item.Value);
                            textEXIF = new string(tab);
                            if (textEXIF.Contains("\0")) textEXIF = textEXIF.Replace("\0", "");
                            string[] splitText = textEXIF.Split(new Char[] { ':', ' ' });
                            if (splitText.Length == 6)
                            {
                                // if (text[4] == ':') text = text.Split'-';
                                textEXIF = splitText[0] + "-" + splitText[1] + "-" + splitText[2] + " " + splitText[3] + "h" + splitText[4] + "m" + splitText[5];
                            }
                            break;
                        }
                    }
                }
                image.Dispose();
                text = text.Replace("%EXIF", textEXIF);
            }
            if (text.Contains("%EXIG"))
            {
                string textEXIF = "";
                System.Drawing.Image image;
                try
                {
                    image = System.Drawing.Image.FromFile(oldName.FullName);
                }
                catch(Exception)
                {
                    return false;
                }
                foreach (System.Drawing.Imaging.PropertyItem item in image.PropertyItems)
                {
                    if (item.Id == 0x9003)
                    {
                        if (item.Type == 2)
                        {
                            char[] tab = ASCIIEncoding.ASCII.GetChars(item.Value);
                            textEXIF = new string(tab);
                            if (textEXIF.Contains("\0")) textEXIF = textEXIF.Replace("\0", "");
                            string[] splitText = textEXIF.Split(new Char[] { ':', ' ' });
                            if (splitText.Length == 6)
                            {
                                // if (text[4] == ':') text = text.Split'-';
                                textEXIF = splitText[2] + "." + splitText[1] + "." + splitText[0].Substring(2, 2) + " " + splitText[3] + "h" + splitText[4] + "m" + splitText[5];
                            }
                            break;
                        }
                    }
                }
                image.Dispose();
                text = text.Replace("%EXIG", textEXIF);
            }
            if (text.Contains("%REVERSEDATE"))
            {
                text = oldName.Name;
                text = text.Substring(8, 2) + "-" + text.Substring(5,2) + "-" + text.Substring(0, 4) + text.Substring(10);
            }
            newName = text + oldName.Extension.ToLower();
            return true;
        }

        private void m_ApplyBt_Click(object sender, RoutedEventArgs e)
        {
            foreach (FileDataSet.FileRow row in this.m_DataSet.File.Rows)
            {
                if (row.Process)
                {
                    System.IO.File.Move(
                        System.IO.Path.Combine(row.DirName, row.OldFileName),
                        System.IO.Path.Combine(row.DirName, row.NewFileName));
                }
            }
        }

        private void m_DirTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            m_CurDirTextBox.Text = (e.NewValue as System.IO.DirectoryInfo).FullName;
        }
    }
}
